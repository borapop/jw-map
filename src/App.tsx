import React, { useEffect, useRef, useState } from 'react';
import './App.css';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Annotation,
  Annotations,
} from 'react-simple-maps'
import report from './report.json'
import { scalePow } from 'd3-scale'
const Color = require("color")
console.log(report)

const ratios = report.map(r => getNumber(r.ratio)).filter(r => !isNaN(r))
console.log(ratios.toString())
const MAX_RATIO = Math.max(...ratios)
const MIN_RATIO = Math.min(...ratios)
console.log(MAX_RATIO, MIN_RATIO)
console.log(ratios.sort((a, b) => a > b ? 1 : -1))

function getNumber(numberOrString: any) {
  if (typeof numberOrString === 'number') {
    return numberOrString
  } else {
    return parseFloat(numberOrString.replace(/,/g, ''));
  }
}

const colorScale = scalePow<string>()
  .exponent(1 / 10)
  .domain([MIN_RATIO + 100, 200, 500, 700, 4000, 50000, MAX_RATIO]) // Max is based on China
  /* tslint:disable-next-line */
  .range(["#3F3CFF", "#3CDCFF", "#3CFF8A", "#FFEB3C", "#FF993C", "#FF3C3C", "#EF3CFF"])

const App: React.FC = () => {
  const [zoom, setZoom] = useState(2)
  const [infoCountry, setInfoCountry] = useState<any>(null)
  function zoomIn() {
    setZoom(zoom * 2)
  }
  function zoomOut() {
    setZoom(zoom / 2)
  }
  let counter = 0
  return (
    <div className="App">
      <div>
        {infoCountry ?
          <div style={{
            position: 'absolute',
            background: 'white',
            left: 10,
            top: 10,
            padding: 10,
            border: '1px solid black',
            overflowY: 'scroll',
            maxHeight: '30%'
          }}>
            {[
              ['Country or Territory:',
                infoCountry.country,],
              ['Population:',
                infoCountry.population,],
              ['2018 Peak Pubs.:',
                infoCountry.peak,],
              ['Ratio, 1 Publisher to:',
                infoCountry.ratio,],
              ['% Inc. Over 2017:',
                infoCountry.percentIncreaseToPastYear,],
              ['2018 No. Bptzd.:',
                infoCountry.baptized,],
              ['Av. Pio. Pubs.:',
                infoCountry.averagePioners,],
              ['No. of Congs.:',
                infoCountry.congregationsNumber,],
              ['Memorial Attendance:',
                infoCountry.memorialAttendance,]
            ].map(([label, value]) =>
              <div style={{ display: 'flex', marginBottom: 5 }}>
                <div style={{ flex: 1, textAlign: 'left', marginRight: 10 }}>{label}</div>
                <div>{value}</div>
              </div>
            )}
          </div>
          :
          <div />
        }
      </div>
      <div style={{
        position: 'absolute',
        top: '45%',
        right: 10,
      }}>
        {[['+', zoomIn], ['–', zoomOut]].map((button: any[], index) => (
          <div
            style={{
              border: '1px solid black',
              fontSize: 25,
              fontWeight: 700,
              marginBottom: 10,
              height: 50,
              width: 50,
              textAlign: 'center',
              lineHeight: '50px',
              borderRadius: '50%',
              backgroundColor: 'white',
            }}
            onClick={button[1]}
          >
            {button[0]}
          </div>
        ))}

      </div>
      <div style={{
        position: 'absolute',
        background: 'white',
        left: 0,
        bottom: 0,
        margin: 10,
        padding: '10px 10px 0 10px',
        border: '1px solid black'
      }}>
        <div style={{ margin: '0 10px 10px 10px' }}>
          Ratio, 1 Publisher to:
        </div>
        <div style={{ display: 'flex', flexWrap: 'wrap' }}>
          {[0, 100, 300, 500, 1000, 2000, 10000, 20000].map(ratio => (
            <div key={ratio} style={{ display: 'flex', marginBottom: 5 }}>

              <div style={{
                margin: '0 5px',
                paddingTop: 5,
                fontWeight: 600,
                borderTop: `10px solid ${ratio === 0 ? '#cccccc' : colorScale(ratio)}`,
              }}>
                {ratio || 'No Info'}
              </div>
            </div>
          ))}

        </div>
      </div>
      <ComposableMap width={window.innerWidth} height={window.innerHeight}>
        <ZoomableGroup zoom={zoom}>
          <Geographies geography={"/world-10m.json"}>
            {(geographies, projection) => geographies.map((geography: any) => {
              console.log(geography)
              const { NAME_LONG: name, ISO_A3: iso } = geography.properties
              const country = report.find((item) => item.iso === iso || item.country === name)
              let countryColor
              let ratio
              if (country) {
                console.log(++counter)
                ratio = getNumber(country.ratio)
                countryColor = colorScale(ratio)
              }
              return (
                <Geography
                  tabable={false}
                  key={name}
                  geography={geography}
                  projection={projection}
                  onClick={(g) => setTimeout(() => setInfoCountry(country), 0)}
                  onMouseEnter={(g) => setTimeout(() => setInfoCountry(country), 0)}
                  onMouseLeave={() => setInfoCountry(null)}
                  style={{
                    default: {
                      fill: countryColor || '#cccccc',
                      stroke: "#444444",
                      strokeWidth: 0.5,
                      outline: "none",
                    },
                  }}
                />
              )
            })}
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
    </div>
  );
}

export default App;
